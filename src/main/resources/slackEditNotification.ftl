[@ww.textfield labelKey="slack.webhookUrl" name="webhookUrl" value="${webhookUrl!}" required='true'/]
[@ww.textfield labelKey="slack.channel" name="channel" value="${channel!}" required='false'/]
[@ww.textfield labelKey="slack.iconUrl" name="iconUrl" value="${iconUrl!}" required='false'/]
[@ww.textfield labelKey="slack.botName" name="botName" value="${botName!}" required='false'/]
